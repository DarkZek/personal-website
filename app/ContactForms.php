<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactForms extends Model
{
  protected $table = "contact_forms";
}
