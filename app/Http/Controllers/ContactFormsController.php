<?php

namespace App\Http\Controllers;

use App\ContactForms;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class ContactFormsController extends Controller
{
    public function index()
    {
        return view("contact")->with('title', 'Marshall Ashdowne | Contact');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $form = new ContactForms();
      $form->title = $request->input('title');
      $form->content = $request->input('content');
      $form->contact = $request->input('contact');
      $form->save();

      return view("contacted")->with('title', 'Marshall Ashdowne | Contact');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ContactForms  $contactForms
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      if (!Auth::check()) {
        return redirect("/");
      }
      $form = \App\ContactForms::findOrFail($id);

      return view("view-contact")->with('form', $form)->with('title', 'Marshall Ashdowne | Contacts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ContactForms  $contactForms
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      if (!Auth::check()) {
        return redirect("/");
      }
      \App\ContactForms::destroy($id);
      return redirect("/home");
    }
}
