function isElementInViewport(elem) {
    var $elem = $(elem);

    // Get the scroll position of the page.
    var scrollElem = ((navigator.userAgent.toLowerCase().indexOf('webkit') != -1) ? 'body' : 'html');
    var viewportTop = $(scrollElem).scrollTop();
    var viewportBottom = viewportTop + $(window).height();

    // Get the position of the element on the page.
    var elemTop = Math.round( $elem.offset().top );
    var elemBottom = elemTop + $elem.height();

    return ((elemTop < viewportBottom) && (elemBottom > viewportTop));
}

// Check if it's time to start the animation.
function checkAnimation() {
    var $elem = $('.exp-item .exp-card');

    // If the animation has already been started
    if ($elem.hasClass('start')) {
      return;
    }

    if (isElementInViewport($elem)) {
        // Start the animation
        $elem.addClass('scene_element');
        $elem.addClass('scene_element--fadeInExpand');
    }
}

function showDiscord() {
  //Fade out title
  $("name-title").addClass("scene_element--fadeinup");
  //Fade in discord
  $("discord").css("hidden", "false");
}

// Capture scroll events
$(window).scroll(function(){
    checkAnimation();
});

function submitEgg(eggName) {
  var url = window.location + "/claim?egg=" + eggName + "&id=" + getCookie("PHPSESSID");

  var xmlHttp = new XMLHttpRequest();
  xmlHttp.open( "GET", url , true );
  xmlHttp.setRequestHeader('Cookie', document.cookie);
  xmlHttp.send( null );
}
function getCookie(name) {
  var value = "; " + document.cookie;
  var parts = value.split("; " + name + "=");
  if (parts.length === 2) {
    return parts.pop().split(";").shift();
  }
}
