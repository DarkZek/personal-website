<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta property="og:type"   content="article" />
        <meta property="og:url"    content="http://shitcoder.cf/"/>
        <meta property="og:title"  content="Marshall Ashdowne | Full Stack Developer" />
        <meta property="og:image"  content="{{ asset('/storage/images/Logo.png') }}" />
        <meta property="og:description"  content="I have a passion for building robust and stylish websites that started 3 years ago. Since then I have been starting and finishing websites to help online communitys, private individuals and open source projects" />
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{csrf_token()}}">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="initial-scale=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="/css/app.css">
        <meta name="viewport" content="initial-scale=1">
        <div id='app'></div>
        <script src="/js/app.js"></script>
        <script src="/storage/js/index.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
        @yield('head')
        <title>{{$title}}</title>
        <link rel="shortcut icon" href="{{{ asset('storage/images/Logo.svg') }}}">
    </head>
    <body>
      @yield('content')

      @extends("inc.footer", ['contactMessage' => isset($contactMessage) ? $contactMessage : true ])
    </body>
</html>
