<div class="footer">
  <div class="container">
    @if($contactMessage)
    <div class="row contact card">
      <a>Get in touch! Tell me about your website and <b>lets do this</b>.</a>
      <a href="/contact" class="btn">Contact Me</a>
    </div>
    @endif
    <div class="row footer-content">
        <div class="col-md-3 desktop-only">
          <h1 class="center white">Location</h1>
          <br>
          <a target="_blank" class="white" href="https://www.google.com/maps/@-43.5214422,170.8442749,7.25z">I currently live and study in the South Island of New Zealand!</a>
        </div>
        <div class="col-1 desktop-only"></div>
        <div class="col-md-4 footer-center">
          <br>
          <div class="row">
            <div class="col-2 col-md-offset-1">
              <a class="linkedin" target="_blank" href="https://www.linkedin.com/in/marshall-ashdowne-746989167/" title="Check out my LinkedIn"><span class="fab fa-linkedin-in"></span></a>
            </div>
            <div class="col-2">
              <a class="twitter" class="desktop-only" target="_blank" href="https://twitter.com/daarkzek" title="Send me a message on twitter"><span class="fab fa-twitter"></span></a>
            </div>
            <div class="col-2">
              <a class="gitlab" target="_blank" href="https://gitlab.com/darkzek" title="My Gitlab"><span class="fab fa-gitlab"></span></a>
            </div>
            <div class="col-2">
              <a class="reddit" class="desktop-only" target="_blank" href="https://reddit.com/u/monster4210" title="Upvote all my posts on reddit"><span class="fab fa-reddit-alien"></span></a>
            </div>
            <div class="col-2">
              <a class="email" target="_blank" href="mailto:daarkzek@protonmail.com" title="Send me an email"><span class="fas fa-envelope"></span></a>
            </div>
          </div>
        </div>
        <div class="col-1 desktop-only"></div>
        <div class="col-3 desktop-only">
          <h1 class="center white">Email</h1>
          <br>
          <a class="white" href="mailto:daarkzek@protonmail.com">Send me an email over at daarkzek@protonmail.com</a>
        </div>
    </div>
  </div>
</div>
