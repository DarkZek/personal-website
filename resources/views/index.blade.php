@extends("inc.app", ['contactMessage', 'false'])

@section("content")
  <div class="title">
    <div class="container top-nav">
      <div class="row">
        <div class="col-12">
          <div class="logo" onclick="document.location = '/';"><object class="" data="{{ asset('/storage/images/Logo.svg') }}" type="image/svg+xml"></object></div>
          <a class="btn right" href="/contact">Contact Me</a>
        </div>
      </div>
    </div>
    <div class="container titles">
      <h1 class="white">Full Stack Developer</h1>
      <a class="white">I build beautiful user interfaces and server side software</a>
    </div>
  </div>
  <div class="content">
    <object class="devices" data="{{ asset('/storage/images/DevicesInkscapeEdited.svg') }}" type="image/svg+xml"></object>
    <div class="intro">
      <div class="container">
        <h1 class="white">Hi, I'm Marshall!</h1>
        <a class="white">I have a passion for building robust and stylish websites that started 3 years ago. Since then I have been starting and finishing websites to help online communities, private individuals and open source projects</a>
      </div>
    </div>
    <div class="container">
      <div class="talents card">
        <div class="row">
          <div class="col-md-4">
            <i class="material-icons">timeline</i>
            <h3>UI/UX Designer</h3><h5>Technologies Used</h5>
            <ul>
              <li><a>Inkscape</a></li>
              <li><a>Krita</a></li>
              <li><a>CSS</a></li>
              <li><a>Firefox</a></li>
            </ul>
            <a class="justify">The challenge of creating a functional but stylish website is what keeps me improving, always improving and adapting to new technology. The user experience is the most important part of making software because it alone can determine if a user uses or abandons the software.</a>
            
          </div>
          <div class="col-md-4 col-wall">
            <i class="material-icons">vertical_split</i>
            <h3>Front End Programming</h3>
            <h5>Technologies Used</h5>
            <ul>
              <li><a>Javascript</a></li>
              <li><a>HTML</a></li>
              <li><a>CSS</a></li>
              <li><a>Bootstrap</a></li>
              <li><a>Node.js</a></li>
            </ul>
            <a>I believe the front end is one of the more important parts of any website so it is very important to get right, keeping styling consistent to ensure your brand is instantly recognisable</a>
          </div>
          <div class="col-md-4 col-wall">
            <i class="material-icons">settings_system_daydream</i>
            <h3>API/Back End Developer</h3>
            <h5>Technologies Used</h5>
            <ul>
              <li><a>PHP/Composer</a></li>
              <li><a>Laravel</a></li>
              <li><a>Linux</a></li>
              <li><a>C#.NET</a></li>
              <li><a>Java</a></li>
              <li><a>Python</a></li>
            </ul>
            <a>I have learnt from many websites that the most important thing when developing back end software is to use a framework. My go to framework is <span class="cursor" onclick="var win = window.open('https://laravel.com/', '_blank');win.focus();" >Laravel</span> and <span class="cursor" onclick="var win = window.open('https://php.net/', '_blank');win.focus();" >PHP</span>, which allows you to make elegant REST API's for web applications and mobile applications alike.</a>
            <br class="desktop-only"><br class="desktop-only">
          </div>
        </div>
      </div>
    </div>
    <div class="container projects">
      <h1 class="center white">Personal Projects</h1>
      <br>
      <div class="row">
        <div class="col-md-4">
          <div class="card" style="background-image: url({{ asset("/storage/images/AshCollAutoLogin.png") }});" onclick="var win = window.open('https://gitlab.com/darkzek/AshcollAutoLogin', '_blank');win.focus();">
            <div class="display">
              <h3>AshcollAutoLogin</h3>
              <a>Is a android app to log users in to the school BYOD at Ashburton College</a>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card" style="background-image: url({{ asset("/storage/images/Opinionated.png") }});" onclick="var win = window.open('https://opinionated.nz/', '_blank');win.focus();">
            <div class="display">
              <h3>Opinionated</h3>
              <a>Opinionated allows New Zealanders to share our opinions on the future of the country</a>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card" style="background-image: url({{ asset("/storage/images/UDC.png") }});" onclick="var win = window.open('https://darkzek.github.io/UDC-Christmas-Event/Website/index.html', '_blank');win.focus();">
            <div class="display">
              <h3>UDC Christmas Event</h3>
              <a>Was an event which contained computer puzzles for users to solve</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <br>
    <br>
  <script>
  //Only use screen size device begins with (since mobile browsers change screen size when scrolling down)
  $(".content")[0].style.top = ($(window).height() + 20) + "px";
  </script>
@endsection


@section("head")
  <script type="text/javascript" src="{{ asset('js/index.js') }}"></script>
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/brands.css" integrity="sha384-1KLgFVb/gHrlDGLFPgMbeedi6tQBLcWvyNUN+YKXbD7ZFbjX6BLpMDf0PJ32XJfX" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/fontawesome.css" integrity="sha384-jLuaxTTBR42U2qJ/pm4JRouHkEDHkVqH0T1nyQXn1mZ7Snycpf6Rl25VBNthU4z0" crossorigin="anonymous">
@endsection
