@extends("inc.app", ['contactMessage' => false])

@section("content")
  <div class="title">
    <div class="container top-nav">
      <div class="row">
        <div class="col-12">
          <div class="logo" onclick="document.location = '/';"><object class="" data="{{ asset('/storage/images/Logo.svg') }}" type="image/svg+xml"></object></div>
          <a class="btn right" href="/contact">Contact Me</a>
        </div>
      </div>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <h1 class="center white">Submitted contact</h1>
      <div class="row">
        <a class="center white" style="width: 100%;">You should recieve a response from me within 1-3 days</a>
      </div>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
    </div>
  </div>
  <script>
  //Only use screen size device begins with (since mobile browsers change screen size when scrolling down)
  $(".content")[0].style.top = ($(window).height() + 20) + "px";
  </script>
@endsection


@section("head")
  <script type="text/javascript" src="{{ asset('js/index.js') }}"></script>
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/brands.css" integrity="sha384-1KLgFVb/gHrlDGLFPgMbeedi6tQBLcWvyNUN+YKXbD7ZFbjX6BLpMDf0PJ32XJfX" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/fontawesome.css" integrity="sha384-jLuaxTTBR42U2qJ/pm4JRouHkEDHkVqH0T1nyQXn1mZ7Snycpf6Rl25VBNthU4z0" crossorigin="anonymous">
@endsection
