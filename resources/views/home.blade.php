@extends('inc.app', ['title' => 'Marshall Ashdowne | Home'])

@section('content')
  <div class="title">
    <div class="container top-nav">
      <div class="row">
        <div class="col-12">
          <div class="logo" onclick="document.location = '/';"><object class="" data="{{ asset('/storage/images/Logo.svg') }}" type="image/svg+xml"></object></div>
          <a class="btn right" href="/contact">Contact Me</a>
        </div>
      </div>
    </div>
  </div>
  <br>
  <br>
  <div class="container">
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <div class="card contacts">
      <h1 class="center">Contacts</h1>
      <?php

      $contacts = \App\ContactForms::all();

      foreach ($contacts as $contact) {
        ?>

        <div class="row cursor grey-hover" onclick="document.location = '/contact/{{ $contact->id }}'">
          <div class="col">
            <a>{{ htmlspecialchars($contact->title) }}</a>
          </div>
        </div>
        <div class="divider"></div>

        <?php
      }

      ?>
    </div>
  </div>
@endsection
