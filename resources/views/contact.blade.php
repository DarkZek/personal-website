@extends("inc.app", ['contactMessage' => false])

@section("content")
  <div class="title">
    <div class="container top-nav">
      <div class="row">
        <div class="col-12">
          <div class="logo" onclick="document.location = '/';"><object class="" data="{{ asset('/storage/images/Logo.svg') }}" type="image/svg+xml"></object></div>
          <a class="btn right" href="/contact">Contact Me</a>
        </div>
      </div>
      <br>
      <form class="form" action="{{ action('ContactFormsController@store') }}" method="post">
        {{ csrf_field() }}
        <h1 class="white">Contact</h1>
        <div class="divider"></div>
        <br>
        <label for="title">Title</label>
        <input type="text" class="form-control" id="title" placeholder="I have this great idea..." name="title">
        <br>
        <label for="content">Content</label>
        <textarea rows="10" name="content"></textarea>
        <br>
        <br>
        <label for="contact">Contact Email</label>
        <input type="email" class="form-control" id="contact" name="contact">
        <br>
        <input type="submit" class="btn form-control" value="Send it off">
      </form>
    </div>
  </div>
  <script>
  //Only use screen size device begins with (since mobile browsers change screen size when scrolling down)
  $(".content")[0].style.top = ($(window).height() + 20) + "px";
  </script>
@endsection


@section("head")
  <script type="text/javascript" src="{{ asset('js/index.js') }}"></script>
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/brands.css" integrity="sha384-1KLgFVb/gHrlDGLFPgMbeedi6tQBLcWvyNUN+YKXbD7ZFbjX6BLpMDf0PJ32XJfX" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/fontawesome.css" integrity="sha384-jLuaxTTBR42U2qJ/pm4JRouHkEDHkVqH0T1nyQXn1mZ7Snycpf6Rl25VBNthU4z0" crossorigin="anonymous">
@endsection
